package struts.config;

import java.util.HashMap;
import java.util.Map;
/**
 * 
 * @author heimu
 */
public class StrutsConfig {
	private static Map<String, ActionConfig> actionMap = new HashMap<String, ActionConfig>();

	public static ActionConfig getActionConfig(String path) {
		return actionMap.get(path);
	}
	
	public  static void addActionConfig(String path,ActionConfig con) {
		actionMap.put(path, con);
	}

}
