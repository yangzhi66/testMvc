package struts.config;

import java.util.Map;

import struts.action.AbstractAction;
import struts.form.BaseForm;

public class ActionConfig {
	private String actionName;
	private String className;
	private String refForm;
	private String path;
	private Map<String,String> resutMap;
	private AbstractAction action;
	public String getActionName() {
		return actionName;
	}
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getRefForm() {
		return refForm;
	}
	public void setRefForm(String refForm) {
		this.refForm = refForm;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public Map<String, String> getResutMap() {
		return resutMap;
	}
	public void setResutMap(Map<String, String> resutMap) {
		this.resutMap = resutMap;
	}
	
	public AbstractAction getAction() {
		return action;
	}
	public void setAction(AbstractAction action) {
		this.action = action;
	}
	public String getUrl(String typeName) {
		return this.resutMap.get(typeName);
	}
	
}
