package struts.parser;

import java.lang.reflect.Field;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import struts.form.BaseForm;

public class ModelParser {
	public static BaseForm parseForm(String className, HttpServletRequest request){
		try {

			Class<?> clazz = Class.forName(className);
			BaseForm form=(BaseForm)clazz.newInstance();
			Enumeration<String> names=request.getParameterNames();
			while(names.hasMoreElements()) {
				String name=names.nextElement();
				Field field=clazz.getDeclaredField(name);
				if(field!=null) {
					field.setAccessible(true);
					field.set(form, request.getParameter(name));
					field.setAccessible(false);
				}
			}
			return form;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("解析form错误:"+e.getMessage());
		}
		return null;
	}
}
