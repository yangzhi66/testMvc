package struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import struts.form.BaseForm;
import struts.form.UserForm;

public class UserAction extends AbstractAction {

	@Override
	public String execute(BaseForm form, HttpServletRequest request, HttpServletResponse response) {
	   UserForm userForm=(UserForm)form;
	   if(userForm.getName().equals("yangz")&&userForm.getPassword().equals("123456")) {
		   request.setAttribute("name", userForm.getName());
		   return "success";
	   }else {
		   request.setAttribute("msg", "账号或密码错误!");
		   return "fail";
	   }
	}

}
