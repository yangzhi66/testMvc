package struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import struts.config.StrutsConfig;
import struts.form.BaseForm;

public abstract class AbstractAction {
	public abstract String execute(BaseForm form,HttpServletRequest request,HttpServletResponse response);
}
